# Save the humans

WPF .NET application/game where you have to move player to portal and avoid aliens.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman.

## Purpose

The purpose of this application is to learn more about WPF applications.

## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.